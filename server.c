#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include "linkedlist.h"
#include "ircstructs.h"
#include "irccommands.h"

// Davis Bolt
// CS 3337

void* receiver(void* arg);
void* sender(void* arg);
void sendAllClientMsg(char* buffer, struct client* c);
void error(char* msg);

struct linkedList clients;
pthread_mutex_t listMutex;

int main(int argc, char** argv){
    int sockfd, newsockfd;
    struct sockaddr_in servaddr, cliaddr;
    int clilen = sizeof(cliaddr);
    clients.size = 0;
    pthread_mutex_init(&listMutex, NULL);

    if (argc < 2)
        error("ERROR, no port provided");

    //open socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    //setup sockaddr
    bzero((char*) &servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(atoi(argv[1]));

    //bind socket to sockfd
    bind(sockfd, (struct sockaddr*) &servaddr, sizeof(servaddr));

    //listen
    listen(sockfd, 5);

    pthread_t sthread;
    pthread_create(&sthread, NULL, sender, NULL);

    printf("If clients can't connect, close and reopen after a little bit\n");

    while (1){
        pthread_t rthread;

        //accept connection
        newsockfd = accept(sockfd, (struct sockaddr*) &cliaddr, &clilen);
        if (newsockfd < 0)
            error("ERROR on accept");

        printf("Client connected\n");

        //create new thread for the connection
        pthread_create(&rthread, NULL, receiver, (void *) newsockfd);
    }

    pthread_mutex_destroy(&listMutex);
    return 0;
}

void* receiver(void* arg){
    char buffer[256];
    char msg[256];
    int n;

    struct client client;
    client.sockfd = (int) arg;
    client.nickName = NULL;
    client.accepted = 0;

    //get password
    read(client.sockfd, buffer, 255);
    parseClientCommand(buffer, &client, &clients);
    if (client.accepted == 1){
        char* accepted = "Password accepted\n\n";
        write(client.sockfd, accepted, strlen(accepted));
    } else {
        char* noPass = "No password given. Disconnecting.\n";
        write(client.sockfd, noPass, strlen(noPass));
        close(client.sockfd);
        return 0;
    }

    //add to list of clients
    pthread_mutex_lock(&listMutex);
    if (clients.size == 0){
        linkedListAddHead(&clients, &client);
    } else {
        linkedListAdd(&clients, &client);
    }
    pthread_mutex_unlock(&listMutex);

    while (1){
        bzero(buffer, 256);

        //get client message
        read(client.sockfd, buffer, 255);
        bcopy(buffer, msg, 256);

        //runs any commands from client's message
        n = parseClientCommand(buffer, &client, &clients);

        if (n == 2){
            pthread_mutex_lock(&listMutex);
            linkedListRemove(&clients, &client);
            pthread_mutex_unlock(&listMutex);
            return 0;
        }

        //print message and send to all clients
        if (client.nickName != NULL && n == 0) {
            printf("%s: %s\n", client.nickName, msg);
            sendAllClientMsg(msg, &client);
        }
    }
}

void* sender(void* arg){
    char buffer[256];
    char msg[270];

    while (1){
        bzero(buffer, 256);
        bzero(msg, 270);
        strcpy(msg,"server: ");

        //get message from user
        fgets(buffer, 255, stdin);
        strcat(msg, buffer);
        strcat(msg, "\n");

        //write message to all client sockets
        pthread_mutex_lock(&listMutex);
        struct listNode* temp = clients.head;
        while (temp != NULL){
            write(((struct client*)temp->data)->sockfd, msg, strlen(msg));
            temp = temp->next;
        }
        pthread_mutex_unlock(&listMutex);

        printf("\n");
    }
}

void sendAllClientMsg(char* buffer, struct client* c){
    char msg[272];
    strcpy(msg,c->nickName);
    strcat(msg, ": ");
    strcat(msg, buffer);
    strcat(msg, "\n");

    pthread_mutex_lock(&listMutex);
    struct listNode* temp = clients.head;
    while (temp != NULL){
        if (((struct client*)temp->data) != c) {
            write(((struct client *) temp->data)->sockfd, msg, strlen(msg));
        }
        temp = temp->next;
    }
    pthread_mutex_unlock(&listMutex);
}

void error(char* msg) {
    printf("%s\n", msg);
    exit(1);
}

