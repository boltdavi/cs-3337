struct client{
	char* nickName;
	char* userName;
	char* realName;
	char* hostName;
    char* serverName;
	char modes[4];
	int accepted;
	int sockfd;
};

struct channel{
	char* name;
	char* topic;
	char* modes;
	struct linkedList ops;
	struct linkedList clients;
};
