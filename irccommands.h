void sendClientMsg(char* buffer, struct client* c, char* receiverNick, struct linkedList* clients){
    char msg[300];
    strcpy(msg, c->nickName);
    strcat(msg, "(whisper): ");
    strcat(msg, buffer);
    strcat(msg, "\n");

    struct listNode* temp = clients->head;
    while (temp != NULL){
        if (strcmp(((struct client*)temp->data)->nickName, receiverNick) == 0){
            write(((struct client*)temp->data)->sockfd, msg, strlen(msg));
            temp = temp->next;
        }
    }
}



void nick(char* token, struct client* c){
    token = strtok(NULL, " \n");
    char* nick = strdup(token);
    c->nickName = nick;
}

void pass(char* token, struct client* c){
    //for now, all passwords will be accepted
    c->accepted = 1;
}

void user(char* token, struct client* c){
    token = strtok(NULL, " \n");
    char* user = strdup(token);
    c->userName = user;

    token = strtok(NULL, " \n");
    char* host = strdup(token);
    c->hostName = host;

    token = strtok(NULL, " \n");
    char* server = strdup(token);
    c->serverName = server;

    token = strtok(NULL, " \n");
    char* real = strdup(token);
    c->realName = real;
}

void oper(){}

void quit(struct client* c){
    close(c->sockfd);
}

void join(){}
void part(){}
void mode(){}
void topic(){}
void names(){}
void list(){}
void invite(){}
void kick(){}

void privmsg(char* token, struct client* c, struct linkedList* clients){
    token = strtok(NULL, " \n");
    char* reciever = strdup(token);
    token = strtok(NULL, "\n");
    char* msg = strdup(token);

    sendClientMsg(msg, c, reciever, clients);
}



int parseClientCommand(char* msg, struct client* c, struct linkedList* clients){
    int retval = 0;
    char* token = strtok(msg, " \n");

    if (strcmp(token, "NICK") == 0){
        nick(token, c);
        retval = 1;

    } else if (strcmp(token, "PASS") == 0){
        pass(token, c);
        retval = 1;

    } else if (strcmp(token, "USER") == 0){
        user(token, c);
        retval = 1;

    } else if (strcmp(token, "OPER") == 0){
        retval = 1;

    } else if (strcmp(token, "QUIT") == 0){
        quit(c);
        retval = 2;

    } else if (strcmp(token, "JOIN") == 0){
        retval = 1;

    } else if (strcmp(token, "PART") == 0){
        retval = 1;

    } else if (strcmp(token, "MODE") == 0){
        retval = 1;

    } else if (strcmp(token, "TOPIC") == 0){
        retval = 1;

    } else if (strcmp(token, "NAMES") == 0){
        retval = 1;

    } else if (strcmp(token, "LIST") == 0){
        retval = 1;

    } else if (strcmp(token, "INVITE") == 0){
        retval = 1;

    } else if (strcmp(token, "KICK") == 0){
        retval = 1;

    } else if (strcmp(token, "PRIVMSG") == 0){
        privmsg(token, c, clients);
        retval = 1;
    }

    return retval;
}