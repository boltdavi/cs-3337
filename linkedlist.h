struct linkedList{
	struct listNode* head;
	int size;
};

struct listNode{
	void* data;
	struct listNode* next;
};

void linkedListAddHead(struct linkedList* list, void* data){
	list->head = (struct listNode*) malloc(sizeof(struct listNode));
	list->head->data = data;
    list->size = 1;
}

void linkedListAdd(struct linkedList* list, void* data){
	struct listNode* current = list->head;
	while (current->next != NULL){
		current = current->next;
	}

	current->next = (struct listNode*) malloc(sizeof(struct listNode));
	current->next->data = data;
	current->next->next = NULL;
		
	list->size++;
}

void linkedListRemove(struct linkedList* list, void* data){
	struct listNode* current = list->head;
	if (current->data == data){
	    list->head = current->next;
	    current->next = NULL;
	    free(current);
	    list->size--;
	} else {
        while (current->next->data != data){
            current = current->next;
        }

        struct listNode* temp = current->next;
        current->next = current->next->next;
        free(temp);

        list->size--;
	}
}

void* linkedListGet(struct linkedList* list, int index){
	struct listNode* current = list->head;
	for (int i = 0; i < index; i++){
		current = current->next;
	}

	return current->data;
}
