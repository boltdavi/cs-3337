#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>

// Davis Bolt
// CS 3337

void* sender(void* arg);
void* receiver(void* arg);
void error(char* msg);

int main(int argc, char** argv){
    int sockfd, n;

    struct sockaddr_in servaddr;
    struct hostent* server;

    if (argc < 3){
        fprintf(stderr, "Usage: %s hostname port\n", argv[0]);
        exit(1);
    }

    //open socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    //get server
    server = gethostbyname(argv[1]);
    if (server == NULL)
        error("ERROR, no such host");

    //setup sockaddr
    bzero((char*) &servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    bcopy((char*) server->h_addr, (char*) &servaddr.sin_addr.s_addr, server->h_length);
    servaddr.sin_port = htons(atoi(argv[2]));

    //connect
    if (connect(sockfd, (struct sockaddr*) &servaddr, sizeof(servaddr)) < 0)
        error("ERROR connecting\n");

    //sender thread
    pthread_t thread;
    pthread_create(&thread, NULL, sender, (void*) sockfd);

    printf("Use PASS <password> to provide connection password (anything typed will be accepted)\n");
    printf("Then use NICK <nickname> to start chatting\n");

    //receive messages
    char buffer[256];
    while (1){
        bzero(buffer, 256);

        //read from socket
        read(sockfd, buffer, 255);
        printf("%s", buffer);
    }

    //return 0;
}

void* sender(void* arg){
    int sockfd = (int) arg;
    char buffer[256];

    while (1){
        //printf("Enter message: ");
        bzero(buffer, 256);

        //get message from user
        fgets(buffer, 255, stdin);

        //write message to socket
        write(sockfd, buffer, strlen(buffer));

        printf("\n");
    }
}

void error(char* msg) {
    printf(msg);
    exit(1);
}